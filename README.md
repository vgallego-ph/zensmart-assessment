# ZenSmart-Assessment

## START 7:00PM (PH Time)

## FINISH 8:19PM (PH Time)
## Got problem on the commit, Sorry I forgot to change the user

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Used Material UI for the layouts

for Installing Material UI - the world's most popular React UI framework.

### `yarn add @mui/material @emotion/react @emotion/styled`

## Available Scripts

In the project directory, you can run:

### `yarn install`
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## The Final work done on this Project

https://gitlab.com/vgallego-ph/zensmart-assessment/-/blob/main/public/final-work.webm
