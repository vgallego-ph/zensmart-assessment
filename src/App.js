import { Typography } from "@mui/material";
import TodoList from "./components/TodoList";
import React from "react";

function App() {
  const createData = (text, done) => {
    return {
      text,
      done,
    };
  };

  const [items, updateItems] = React.useState([
    createData("Task1", false),
    createData("Task2", false),
    createData("Task3", true),
    createData("Task4", false),
    createData("Task5", false),
  ]);

  const updateToNewItems = React.useCallback((items) => updateItems(items), []);

  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);

  const onItemClick = (key) => {
    const newItem = items[key];
    newItem.done = true;
    items[key] = newItem;
    updateToNewItems(items);
    forceUpdate();
  };

  return (
    <React.Fragment>
      <Typography variant="h3" padding={"16px"}>
        Task Lists
      </Typography>
      <TodoList items={items} onItemClick={onItemClick} />
    </React.Fragment>
  );
}

export default App;
