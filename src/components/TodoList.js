import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import * as PropTypes from "prop-types";
import ButtonComponent from "./ButtonComponent";

export default function TodoList({ items, onItemClick }) {
  return (
    <TableContainer
      component={Paper}
      sx={{ width: "fit-content", margin: "16px" }}
    >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell sx={{ fontWeight: "bold", width: "24px" }}>No.</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Task</TableCell>
            <TableCell sx={{ fontWeight: "bold" }}>Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items?.map((item, key) => {
            return (
              <TableRow key={key}>
                <TableCell>{key+1}</TableCell>
                <TableCell>{item.text}</TableCell>
                <TableCell>
                  <ButtonComponent
                    variant="contained"
                    size="large"
                    backgroundColor="#0b6bf2"
                    disabled={item.done}
                    onClick={() => onItemClick(key)}
                    buttonLabel="Done"
                  />
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

TodoList.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      done: PropTypes.bool,
    })
  ),
  onItemClick: PropTypes.func,
};
