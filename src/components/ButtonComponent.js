import * as PropTypes from "prop-types";
import { Button } from "@mui/material";

export default function ButtonComponent({
  buttonLabel,
  variant,
  size,
  backgroundColor,
  disabled,
  onClick,
}) {
  // backgound color if button is inactive/disabled
  const disableBG = "#dbe9fd !important";
  return (
    <Button
      variant={variant}
      size={size}
      sx={{
        background: disabled ? disableBG : backgroundColor + " !important",
        color: "white !important",
        "&:hover": {
          backgroundColor: "#0243eb !important",
        },
      }}
      disabled={disabled}
      onClick={onClick}
    >
      {buttonLabel}
    </Button>
  );
}

ButtonComponent.propTypes = {
  buttonLabel: PropTypes.string,
  variant: PropTypes.string,
  size: PropTypes.string,
  backgroundColor: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};
